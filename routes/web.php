<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 
Route::get ( '/', function () {
    return view ( 'auth.register' );
} );
Route::post ( '/login', 'MainController@login' )->name('login');
Route::post ( '/register', 'MainController@register' )->name('register');
 
Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('login');
Route::post('/admin/dashboard', 'MainController@login')->name('admin.login.submit');
 



Route::get('/home', 'DashboardController@index')->name('home');


Route::get('/admin', 'AdminController@Admin')->name('admin');
Route::get('/admin/add', 'AdminController@AdminAdd')->name('adminAdd');
Route::get('/admin/edit/{iAdminID}','AdminController@EditAdmin')->name('edit.admin');
Route::post('/admin/destroy/{iAdminID}', 'AdminController@DestroyAdmin')->name('destroy.admin');
Route::post('/admin/update/{iAdminID}','AdminController@UpdateAdmin')->name('update.admin');
Route::post('/admin/store','AdminController@StoreAdmin')->name('store.admin');


Route::get('/agent_verifiaction_code', 'AgentController@AgentVerification')->name('agent.verification');
Route::post('/agent_verifiaction_code/entries', 'AgentController@AgentVerification')->name('agent.verification.entries');
Route::get('/Agent_verifiaction_code/add', 'AgentController@AddAgentverificationcode')->name('agentverificationcodeadd');
Route::post('/Agent_verifiaction_code/store','AgentController@StoreAgentverificationcode')->name('store.Agent_verifiaction_code');
Route::get('/Agent_verifiaction_code/edit/{iAgentID}','AgentController@EditAgentverificationcode')->name('edit.Agent_verifiaction_code');
Route::post('/Agent_verifiaction_code/update/{iAgentID}','AgentController@UpdateAgentverificationcode')->name('update.Agent_verifiaction_code');
Route::get('/Agent_verifiaction_code/delete/{iAgentID}','AgentController@DeleteAgentverificationcode')->name('delete.Agent_verifiaction_code');
Route::post('/Agent_verifiaction_code/destroy/{iAgentID}', 'AgentController@DestroyAgentverificationcode')->name('destroy.Agent_verifiaction_code');
Route::post('/Agent_verifiaction_code/active/{iAgentID}', 'AgentController@ActiveAgentverificationcode')->name('active.Agent_verifiaction_code');
Route::post('/Agent_verifiaction_code/inactive/{iAgentID}', 'AgentController@InactiveAgentverificationcode')->name('inactive.Agent_verifiaction_code');



Route::get('/skill', 'SkillController@Skill')->name('skill');
Route::post('/skill/entries', 'SkillController@Skill')->name('skill.entries');
Route::get('/skill/add', 'SkillController@SkillAdd')->name('SkillAdd');
Route::get('/skill/import', 'SkillController@SkillImport')->name('SkillImport');
Route::get('/skill/participation_role', 'SkillController@ParticipationRole')->name('participation.role');


Route::get('/hospital&type', 'HospitalController@Hospital')->name('hospital');
Route::post('/hospital&type/entries', 'HospitalController@Hospital')->name('hospital.entries');
Route::get('/hospital&type/add', 'HospitalController@HospitalAdd')->name('hospital.add');
Route::post('/hospital&type/store','HospitalController@Storehospitaltype')->name('store.hospital&type');
Route::get('/hospital&type/edit/{iHospitalID}','HospitalController@Edithospitaltype')->name('edit.hospital&type');
Route::post('/hospital&type/update/{iHospitalID}','HospitalController@Updatehospitaltype')->name('update.hospital&type');
Route::get('/hospital&type/delete/{iHospitalID}','HospitalController@Deletehospitaltype')->name('delete.hospital&type');
Route::post('/hospital&type/destroy/{iHospitalID}', 'HospitalController@Destroyhospitaltype')->name('destroy.hospital&type');
Route::post('/hospital&type/active/{iHospitalID}', 'HospitalController@Activehospitaltype')->name('active.hospital&type');
Route::post('/hospital&type/inactive/{iHospitalID}', 'HospitalController@Inactivehospitaltype')->name('inactive.hospital&type');
// Location type
Route::get('/hospital&type/location_type', 'HospitalController@LocationType')->name('hospital.location_type');
Route::get('/hospital&type/add_location_type', 'HospitalController@HospitalAddLocationType')->name('hospital.add_location_type');
Route::post('/location_type/store','HospitalController@StoreLocationType')->name('store.location&type');
Route::get('/location_type/edit/{iTypeID}','HospitalController@EditLocationtype')->name('edit.location&type');
Route::post('/location_type/update/{iTypeID}','HospitalController@UpdateLocationtype')->name('update.location&type');
Route::get('/location_type/delete/{iTypeID}','HospitalController@DeleteLocationType')->name('delete.location&type');
Route::post('/location_type/destroy/{iTypeID}', 'HospitalController@DestroyLocationType')->name('destroy.location&type');
Route::post('/location_type/active/{iTypeID}', 'HospitalController@ActiveLocationtype')->name('active.location&type');
Route::post('/location_type/inactive/{iTypeID}', 'HospitalController@InactiveLocationtype')->name('inactive.location&type');
// hospital & type Import
Route::get('/hospital&type/import', 'HospitalController@HospitalImport')->name('hospital.import');
 

Route::get('/school', 'SchoolController@School')->name('school');
Route::get('/school/add', 'SchoolController@SchoolAdd')->name('school.add');
Route::post('/school/store','SchoolController@StoreSchool')->name('store.school');
Route::get('/school/edit/{iSchoolID}','SchoolController@EditSchool')->name('edit.school');
Route::post('/school/update/{iSchoolID}','SchoolController@UpdateSchool')->name('update.school');
Route::get('/school/delete/{iSchoolID}','SchoolController@DeleteSchool')->name('delete.school');
Route::post('/school/destroy/{iSchoolID}', 'SchoolController@DestroySchool')->name('destroy.school');

Route::get('/program', 'ProgramController@Program')->name('program');
Route::get('/program/add', 'ProgramController@ProgramAdd')->name('program.add');
Route::post('/program/store','ProgramController@StoreProgram')->name('store.program');
Route::get('/program/edit/{iProgramID}','ProgramController@EditProgram')->name('edit.program');
Route::post('/program/update/{iProgramID}','ProgramController@UpdateProgram')->name('update.program');
Route::post('/program/destroy/{iProgramID}', 'ProgramController@DestroyProgram')->name('destroy.program');


Route::get('/instructor', 'InstructorController@Instructor')->name('instructor');
Route::get('/instructor/view/{iInstructorID}','InstructorController@ViewInstructor')->name('view.instructor');
Route::get('/instructor/add', 'InstructorController@InstructorAdd')->name('instructor.add');
Route::get('/instructor/edit/{iInstructorID}','InstructorController@EditInstructor')->name('edit.instructor');
Route::post('/instructor/update/{iProgramID}','InstructorController@UpdateInstructor')->name('update.instructor');
Route::post('/instructor/destroy/{iInstructorID}', 'InstructorController@DestroyInstructor')->name('destroy.instructor');



Route::get('/formativeform', 'FormativeController@FormativeForm')->name('formativeform');
Route::get('/formativeform/add', 'FormativeController@FormativeFormAdd')->name('formativeform.add');
Route::get('/formativeform/details/{iInstructorID}','FormativeController@FormativeFormSchools')->name('form.details');


Route::get('/student_learning_contract', 'StudentLearningContractController@StudentLearningContract')->name('student.learning.contract');


Route::get('/student', 'StudentController@Student')->name('student');
Route::get('/student/graduated_students', 'StudentController@GraduatedStudents')->name('graduated.students');
Route::get('/student/view/{iStudentID}','StudentController@ViewStudent')->name('view.student');
Route::get('/student/add', 'StudentController@StudentAdd')->name('student.add');
Route::get('/student/edit/{iStudentID}','StudentController@EditStudent')->name('edit.student');
Route::post('/student/update/{iStudentID}','StudentController@UpdateStudent')->name('update.student');
Route::post('/student/destroy/{iStudentID}', 'StudentController@DestroyStudent')->name('destroy.student');


Route::get('/session', 'SessionController@Session')->name('session');


Route::get('/terms', 'TermsController@Terms')->name('terms');
Route::get('/terms/add', 'TermsController@TermsAdd')->name('terms.add');


Route::get('/code', 'CodeController@Code')->name('code');
Route::get('/code/add', 'CodeController@CodeAdd')->name('code.add');


Route::get('/subscription_management', 'SubscriptionController@SubscriptionManagement')->name('subscription.management');
Route::get('/subscription_management/add', 'SubscriptionController@SubscriptionManagementAdd')->name('subscription_management.add');


Route::get('/learning_center', 'LearningController@LearningCenter')->name('learning.center');
Route::get('/learning_center/add', 'LearningController@LearningCenterAdd')->name('learning_center.add');


Route::get('/clinical_wallet', 'ClinicalWalletController@ClinicalWallet')->name('clinical.wallet');
Route::get('/rubric', 'RubricController@Rubric')->name('rubric');
Route::get('/evaluation', 'EvaluationController@Evaluation')->name('evaluation');

Route::get('/remediation', 'RemediationController@Remediation')->name('remediation');
Route::get('/remediation/view/{iRemediationID}','RemediationController@ViewRemediation')->name('view.remediation');

Route::get('/contact_us', 'ContactUsController@ContactUs')->name('contact.us');
Route::get('/settings', 'SettingsController@Settings')->name('settings');
Route::get('/feedback', 'FeedbackController@Feedback')->name('feedback');

Route::get('/content', 'ContentController@Content')->name('content');
Route::get('/content/edit/{iPageID}','ContentController@EditContent')->name('edit.content');
Route::post('/content/update/{iPageID}','ContentController@UpdateContent')->name('update.content');

Route::post('/search','SearchController@Search')->name('search');



 

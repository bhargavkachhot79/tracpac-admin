@extends('layouts.app')
@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <a class="btn btn-default btn-primary btn-sm selectall right"><span> Export</span></a>        
            <h1>School Management</h1>
            <h6>School name</h6>
            
          </div>
          <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Admin Management</li>
            </ol>
          </div> -->
        </div>
      </div><!-- /.container-fluid -->
    </section>
     @if (session('status'))
        <div class="alert alert-success center success">
              {{ session('status') }}
        </div>
     @endif 

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">


						Show   <select>
						  <option value="10">10</option>
						  <option value="25">25</option>
						  <option value="50">50</option>
						  <option value="100">100</option>
						</select> entries
						 
						<div class="dt-buttons float-right col-sm-8"> 
							<div style="float: right;">	 
								<form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form> 
        </div>
						</div> 
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>School Name</th>                  
                  <th>Address</th>
                  <th>Email</th>
                  <th>Created Date &amp; Time</th>                   
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @if(count($schools))                   
                    @foreach($schools as $school)
                <tr>
                  <td>{{ $school->vSchoolName }}</td>
                  <td>{{ $school->tAddress }}</td>
                  <td>{{ $school->vEmail }}</td>
                  <td>{{ $school->dtAdded }}</td>                   
                  <td>
                  	<a class="btn btn-default btn-primary btn-sm selectall"><span> Verified</span></a>
                  </td>
                  <td>
                  	<a href="{{route('edit.school',$school->iSchoolID)}}" class="btn btn-default btn-primary btn-sm selectall"><span><i class="fas fa-edit"></i> Edit</span></a>
                    <a href="#delete-{{$school->iSchoolID }}" data-toggle="modal" class="btn btn-default btn-danger btn-sm delbtn delete" ><span><i class="fa fa-trash" aria-hidden="true"></i> Delete</span></a>
                     <div href="#delete-{{$school->iSchoolID }}" data-toggle="modal" class="modal" id="delete-{{$school->iSchoolID }}" tabindex="-1" role="dialog">
                              <div class="modal-dialog" role="document">
                                 <div class="modal-content">
                                     
                                     <form action="{{route('destroy.school',$school->iSchoolID)}}" method="POST">
                                       {{ csrf_field() }}
                                    <div class="modal-header">
                                       <h4 class="modal-title">Delete user</h4>
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                       <span aria-hidden="true">&times;</span>
                                       </button>
                                    </div>
                                    <div class="modal-body">
                                       <label>Are You Sure You Want To Delete?</label>
                                    </div>
                                    <div class="modal-footer">
                                       <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">No</button>
                                       <input type="submit" class="btn btn-primary" value="Yes" />
                                      </form>
                                    </div>
                                    
                                 </div>
                              </div> 
                  </td>
                </tr>
                  @endforeach
                  @else 
                    <tr><td colspan="6">No users found</td></tr>
                  @endif 
                </tbody>
                <tfoot>
                <tr>
                  <th>School Name</th>                  
                  <th>Address</th>
                  <th>Email</th>
                  <th>Created Date &amp; Time</th>                   
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
             	 <h6> Showing 1 to 7 of {{count($schools)}} entries</h6>
            </div>

            <!-- /.card-body -->
          </div>
          <!-- /.card -->          
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  @endsection
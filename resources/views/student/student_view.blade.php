@extends('layouts.app')
@section('content')
  @foreach($student as $value)
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{$value->vFirstName}} {{$value->vLastName}}</h1>
          </div>
          <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Advanced Form</li>
            </ol>
          </div> -->
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->

    <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
           
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-2"> 
                  <label>Name:</label> 
              </div>
              <div class="col-md-10"> 
                <span >{{$value->vFirstName}} {{$value->vLastName}}</span> 
              </div>
            </div>
            <div class="row">
              <div class="col-md-2"> 
                  <label>Email :</label> 
              </div>
              <div class="col-md-10"> 
                <span >{{$value->vEmail}}</span> 
              </div>
            </div>
            <div class="row">
              <div class="col-md-2"> 
                  <label>Student ID :</label> 
              </div>
              <div class="col-md-10"> 
                <span >{{ $value->vStudentID }}</span> 
              </div>
            </div>
            <div class="row">
              <div class="col-md-2"> 
                  <label>School Name:</label> 
              </div>
              <div class="col-md-10"> 
                <span >{{$value->tbl_school['vSchoolName']}}</span> 
              </div>
            </div>
            <div class="row">
              <div class="col-md-2"> 
                  <label>Program :</label> 
              </div>
              <div class="col-md-10"> 
                <span >{{$value->tbl_program['vProgramName']}}</span> 
              </div>
            </div> 
            <div class="row">
              <div class="col-md-2"> 
                   <label>Contact No :</label> 
              </div>
              <div class="col-md-10"> 
                <span >{{$value->vPhone}}</span> 
              </div>
            </div>
            <div class="row">
              <div class="col-md-2"> 
                   <label>Profile pic :</label> 
              </div>
              <div class="col-md-10"> 
                <span ><img src="{{$value->vImage}}"></span> 
              </div>
            </div>
            <div class="row">
              <div class="col-md-2"> 
                 <label>Payment History</label>
              </div>

              <div class="col-md-10"> 
                <span >{{$value->vInstructorCode}}</span> 
                
              <table id="example2" class="table table-bordered table-hover">
                <thead class="hedercolor">
                <tr>
                  <th>Description</th>
                  <th>Amount Paid</th>
                  <th>Payment Method</th>
                  <th>Payer Email</th>
                  <th>Payment Date</th>
                  <th>Subscription End Date</th>
                  <th>Action</th>  
                </tr>
                </thead>
                <tbody>
                   @if(count($student))                   
                    @foreach($student as $student)
                <tr>
                  <td>{{ $student->tbl_purchase['tDescription']}}</td>
                  <td>{{ $student->tbl_purchase['iAmount']}}</td>
                  <td>{{ $student->tbl_purchase['vPaymentMethod']}}</td>
                  <td>{{ $student->tbl_purchase['PayerMail']}}</td>
                  <td>{{ $student->tbl_purchase['dtPurchase']}}</td>
                  <td>{{ $student->tbl_purchase['dtEndSubscription']}}</td> 
                  <td>                 
                    <a href="#delete-{{$student->iStudentID }}" data-toggle="modal" class="btn btn-default btn-danger btn-sm delbtn delete" ><span>End Subscription</span></a>
                     <div class="modal" id="delete-{{$student->iStudentID }}" tabindex="-1" role="dialog">
                              <div class="modal-dialog" role="document">
                                 <div class="modal-content">
                                     
                                     <form action="{{route('destroy.student',$student->iStudentID)}}" method="POST">
                                       {{ csrf_field() }}
                                    <div class="modal-header">
                                       <h4 class="modal-title">End Subscription</h4>
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                       <span aria-hidden="true">&times;</span>
                                       </button>
                                    </div>
                                    <div class="modal-body">
                                       <label>Are You Sure You Want To End Subscription?</label>
                                    </div>
                                    <div class="modal-footer">
                                       <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">No</button>
                                       <input type="submit" class="btn btn-primary" value="Yes" />
                                      </form>
                                    </div>
                                    
                                 </div>
                              </div>  

                  </td>
                </tr>                 
                 @endforeach
                  @else 
                    <tr><td colspan="6">No users found</td></tr>
                  @endif 
                </tbody> 
              </table>
               <a href="#delete-{{$student->iStudentID }}" data-toggle="modal" class="btn btn-default btn-danger btn-sm delbtn delete" ><span>Permanent Delete</span></a> 
            </div>

            </div> 
              </div> 
            </div>
            <!-- /.row -->
 
          </div>
           
        </div>
        <!-- /.card -->
 
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div> 
  @endforeach
<!-- Page script -->
 
 @endsection

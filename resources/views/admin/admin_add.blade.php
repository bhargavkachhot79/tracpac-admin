@extends('layouts.app')
@section('content')
 
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Admin Management</h1>
          </div>
          <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Advanced Form</li>
            </ol>
          </div> -->
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <form action="{{route('store.admin' )}}" method="POST">
           {{ csrf_field() }} 
          <div class="card-header">
            <h3 class="card-title">Add Admin</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
              
                <div class="form-group">                    
                  <label>First Name<span class="required">*</span></label>
                  <input type="text" class="form-control my-colorpicker1" name="vFirstName"   placeholder="First name..."> 
                   @if ($errors->has('vFirstName'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('vFirstName') }}</strong>
                                      </span>
                             @endif                  
                </div>
                <!-- /.form-group -->
                <div class="form-group">                    
                  <label>Email<span class="required">*</span></label>
                  <input type="email" class="form-control my-colorpicker1" name="vEmail"  placeholder="Email..."> 
                   @if ($errors->has('vEmail'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('vEmail') }}</strong>
                                      </span>
                             @endif                  
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="form-group">                    
                  <label>Lastname<span class="required">*</span></label>
                  <input type="text" class="form-control my-colorpicker1" name="vLastName"   placeholder="Lastname...">
                   @if ($errors->has('vLastName'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('vLastName') }}</strong>
                                      </span>
                             @endif                   
                </div>
                <!-- /.form-group -->
                <div class="form-group">                    
                  <label>Password<span class="required">*</span></label>
                  <input type="Password" class="form-control my-colorpicker1" name="vPassword" placeholder="*****">  
                   @if ($errors->has('vPassword'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('vPassword') }}</strong>
                                      </span>
                             @endif                 
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
           
            </div>
            <!-- /.row -->
 
          </div>
          <!-- /.card-body -->
        <div class="card-footer">                      
              <a class="btn btn-default btn-primary btn-sm selectall left" href="{{route('home')}}"><span>Cancle</span></a>
              <input type="submit" value="Save" class="btn btn-default btn-primary btn-sm selectall right"> 
          </div>
          </form>
        </div>
        <!-- /.card -->
 
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div> 
<!-- Page script -->
 
 @endsection

@extends('layouts.app')
@section('content')
  @foreach($instructor as $value)
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{$value->vFirstName}} {{$value->vLastName}}</h1>
          </div>
          <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Advanced Form</li>
            </ol>
          </div> -->
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->

    <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
           
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-2"> 
                  <label>School Name:</label> 
              </div>
              <div class="col-md-10"> 
                <span >{{$value->tbl_school['vSchoolName']}}</span> 
              </div>
            </div>
            <div class="row">
              <div class="col-md-2"> 
                  <label>Program :</label> 
              </div>
              <div class="col-md-10"> 
                <span >{{$value->tbl_program['vProgramName']}}</span> 
              </div>
            </div>
            <div class="row">
              <div class="col-md-2"> 
                   <label>Name :</label> 
              </div>
              <div class="col-md-10"> 
                <span >{{$value->vFirstName}} {{$value->vLastName}}</span> 
              </div>
            </div>
            <div class="row">
              <div class="col-md-2"> 
                  <label>Email :</label> 
              </div>
              <div class="col-md-10"> 
                <span >{{$value->vEmail}}</span> 
              </div>
            </div>
            <div class="row">
              <div class="col-md-2"> 
                   <label>Credential :</label> 
              </div>
              <div class="col-md-10"> 
                <span >{{$value->vCredentials}}</span> 
              </div>
            </div>
            <div class="row">
              <div class="col-md-2"> 
                 <label>Instructor Code :</label>
              </div>
              <div class="col-md-10"> 
                <span >{{$value->vInstructorCode}}</span> 
              </div>
            </div> 
              </div> 
            </div>
            <!-- /.row -->
 
          </div>
           
        </div>
        <!-- /.card -->
 
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div> 
  @endforeach
<!-- Page script -->
 
 @endsection

@extends('layouts.app')
@section('content')
 
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Instructor Management</h1>
          </div>
          <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Advanced Form</li>
            </ol>
          </div> -->
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Add Instructor</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">                    
                  <label>First Name :</label>
                  <input type="text" class="form-control my-colorpicker1" placeholder="First name...">
                    </div>
                <div class="form-group">                    
                  <label>Email :</label>
                  <input type="email" class="form-control my-colorpicker1" placeholder="Email...">
                </div>
               
                <!-- /.form-group -->
                <div class="form-group">                    
                  <label>Program :</label>
                  <input type="text" class="form-control my-colorpicker1" placeholder="Program...">
                </div>
                  <div class="form-group">                    
                  <label>Group :</label>
                  <input type="text" class="form-control my-colorpicker1" placeholder="Group...">
                </div>
                <div class="form-group">                    
                  <label>Instructor Code :</label>
                  <input type="text" class="form-control my-colorpicker1" placeholder="Instructor Code...">
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="form-group">                    
                  <label>Lastname :</label>
                  <input type="text" class="form-control my-colorpicker1" placeholder="Lastname...">                
                </div>
                 <div class="form-group">                    
                  <label>School :</label>
                  <input type="text" class="form-control my-colorpicker1" placeholder="First name...">                
                </div>
                <div class="form-group">                    
                  <label>Term:</label>
                  <input type="text" class="form-control my-colorpicker1" placeholder="Term...">                
                </div>
                <!-- /.form-group -->
               
                <div class="form-group">                    
                  <label>Credential :</label>
                  <input type="text" class="form-control my-colorpicker1" placeholder="Credential...">
                </div>
                <div class="form-group">                    
                  <label>Profile pic :</label>
                  <input type="file" class="form-control my-colorpicker1" placeholder="Profile Pic...">
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
 
          </div>
          <!-- /.card-body -->
          <div class="card-footer">                      
              <a class="btn btn-default btn-primary btn-sm selectall left" href="{{route('home')}}"><span>Cancle</span></a>
              <a class="btn btn-default btn-primary btn-sm selectall right"><span> Save</span></a>
          </div>
        </div>
        <!-- /.card -->
 
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div> 
<!-- Page script -->
 
 @endsection

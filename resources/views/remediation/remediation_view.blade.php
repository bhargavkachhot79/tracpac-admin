@extends('layouts.app')
@section('content')
  @foreach($remediation as $value)
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ $value->tbl_student['vFirstName']}} {{ $value->tbl_student['vLastName']}}</h1>
          </div>
          <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Advanced Form</li>
            </ol>
          </div> -->
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->

    <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
           
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-2"> 
                  <label>Student Name:</label> 
              </div>
              <div class="col-md-10"> 
                <span >{{ $value->tbl_student['vFirstName']}} {{ $value->tbl_student['vLastName']}}</span> 
              </div>
            </div>
            <div class="row">
              <div class="col-md-2"> 
                  <label>Instructor Name :</label> 
              </div>
              <div class="col-md-10"> 
                <span >{{ $value->tbl_instructor['vFirstName']}} {{ $value->tbl_instructor['vLastName']}}</span> 
              </div>
            </div>
            <div class="row">
              <div class="col-md-2"> 
                   <label>Category :</label> 
              </div>
              <div class="col-md-10"> 
                <span >{{ $value->vCategory }}</span> 
              </div>
            </div>
            <div class="row">
              <div class="col-md-2"> 
                  <label>Improvement :</label> 
              </div>
              <div class="col-md-10"> 
                <span >{{$value->tImprovement}}</span> 
              </div>
            </div>
            <div class="row">
              <div class="col-md-2"> 
                   <label>Comment :</label> 
              </div>
              <div class="col-md-10"> 
                <span >{{$value->tComment}}</span> 
              </div>
            </div> 
              </div> 
            </div>
            <!-- /.row -->
 
          </div>
           
        </div>
        <!-- /.card -->
 
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div> 
  @endforeach
<!-- Page script -->
 
 @endsection

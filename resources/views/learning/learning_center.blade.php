@extends('layouts.app')
@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
                   
            <h1>Learning Center</h1>
            <h6>Title, Files, Content</h6> 
            
          </div>
          <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Admin Management</li>
            </ol>
          </div> -->
        </div>
      </div><!-- /.container-fluid -->
    </section>
     @if (session('status'))
        <div class="alert alert-success center success">
              {{ session('status') }}
        </div>
     @endif

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">


						Show   <select>
						  <option value="10">10</option>
						  <option value="25">25</option>
						  <option value="50">50</option>
						  <option value="100">100</option>
						</select> entries
						 
						<div class="dt-buttons float-right col-sm-8">
							 
 


							<div style="float: right;">	 
								<form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form> 
        </div>
						</div>
						

						
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Name</th>                  
                  <th>Content</th>
                  <th>Content Type</th>
                  <th>Created Date &amp; Time</th> 
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @if(count($learnings))                   
                    @foreach($learnings as $learning)
                <tr>
                  <td>{{ $learning->vTitle }}</td>
                   
                    @if($learning->tType =='video')         
                        <td><a class="btn btn-default btn-primary btn-sm verified"><span> video</span></a> </td>         
                    @elseif($learning->tType =='guide')
                        <td><a class="btn btn-default btn-primary btn-sm verified"><span>guide </span></a></td>
                     @else($learning->tType =='pdf')
                        <td><a class="btn btn-default btn-primary btn-sm verified"><span>pdf </span></a></td>                
                    @endif
                   <td>{{ $learning->tType }}</td>
                  <td>{{ $learning->dtAdded }}</td>                   
                  
                  <td>
                  	<a class="btn btn-default btn-primary btn-sm verified"><span> View</span></a>
                  	<a class="btn btn-default btn-primary btn-sm selectall"><span><i class="fas fa-edit"></i> Edit</span></a>
                  	<a class="btn btn-default btn-danger btn-sm delbtn delete" ><span><i class="fa fa-trash" aria-hidden="true"></i> Delete</span></a>	

                  </td>
                </tr>
                @endforeach
                  @else 
                    <tr><td colspan="6">No users found</td></tr>
                  @endif
                </tbody>
                <tfoot>
                <tr>
                  <th>Name</th>                  
                  <th>Content</th>
                  <th>Content Type</th>
                  <th>Created Date &amp; Time</th>                   
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
             	 <h6> Showing 1 to 7 of 7 entries</h6>
            </div>

            <!-- /.card-body -->
          </div>
          <!-- /.card -->          
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  @endsection
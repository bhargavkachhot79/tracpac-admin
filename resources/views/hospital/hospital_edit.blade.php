@extends('layouts.app')
@section('content')
 
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Hospital Management</h1>
          </div>
          <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Advanced Form</li>
            </ol>
          </div> -->
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
           <form action="{{route('update.hospital&type', $iHospitalID )}}" method="POST">
           {{ csrf_field() }} 
          <div class="card-header">
            <h3 class="card-title">Edit Hospital</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                 @foreach($hospital as $value)
                 <div class="form-group">                    
                  <label>Name<span class="required">*</span></label>
                  <input type="text" name="vHostpitalName" value="{{$value->vHostpitalName}}" class="form-control my-colorpicker1" placeholder="Name...">                
                </div>                 
                <div class="form-group">                    
                  <label>Address<span class="required">*</span></label>
                  <input type="text" name="tAddress" id="addressinput" value="{{$value->tAddress}}" class="form-control my-colorpicker1" placeholder="Address...">              
                </div>
                <div class="form-group">
                <input type="text" name="dLong" id="dLong" class="displaynone" >       
                 <input type="text" name="dLat" id="dLat" class="displaynone "  >  
                      <li class="displaynone">Latitude: <span id="lat-span" class="lat-span"></span></li>
                      <li class="displaynone">Longitude: <span id="lon-span" class="lon-span"></span></li>
                      <li class="displaynone">Address: <span id="address" class="address"></span></li> 
                      <li id="Lat" class="defaultLat displaynone">{{$value->dLat}}</li>
                      <li id="Long" class="defaultLong displaynone">{{$value->dLong}}</li>      
                </div>                 
                <!-- /.form-group -->
                
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                
                <div class="form-group">
                  <label>Hospital Type<span class="required">*</span></label>
                  <div class="form-check">
                    <input type="checkbox" @if($value['tTypeID'] == '5')   checked="checked" @endif name="checkbox[]" value="5" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">CLA</label>
                  </div>
                   <div class="form-check">
                    <input type="checkbox"  @if($value['tTypeID'] == '4')   checked="checked" @endif name="checkbox[]" value="4" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Classroom</label>
                  </div>
                   <div class="form-check">
                    <input type="checkbox" @if($value['tTypeID'] == '1')   checked="checked" @endif class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" name="checkbox[]" value="1" for="exampleCheck1">Clinical</label>
                  </div>
                   <div class="form-check">
                    <input type="checkbox" name="checkbox[]"  @if($value['tTypeID'] == '3')   checked="checked" @endif    value="3" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Lab</label>
                  </div>
                   <div class="form-check">
                    <input type="checkbox" @if($value['tTypeID'] == '2')   checked="checked" @endif name="checkbox[]" value="2" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Simulation</label>
                  </div>
                </div>
                <!-- /.form-group -->              
                @endforeach
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
             <div id="map"></div>

            </div>
            <!-- /.row -->
 
          </div>
          <!-- /.card-body -->
          <div class="card-footer">                      
              <a class="btn btn-default btn-primary btn-sm selectall left" href="{{route('home')}}"><span>Cancle</span></a>
               <input type="submit" value="Save" class="btn btn-default btn-primary btn-sm selectall right">
          </div>
          </form>
        </div>
        <!-- /.card -->
 
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div> 
<!-- Page script -->

 <script language="javascript" src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDR5eGlaHASGJ4S4G4GCtwcZMwmDRu_NJU"></script>
    <script type="text/javascript">
        window.onload = function () {
          var dLat = $('#Lat').text();
          console.log(dLat);
          var dLong = $('#Long').text();
          console.log(dLong);
            var mapOptions = {
                center: new google.maps.LatLng(dLat, dLong),
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
           
            // var myLatLng = {lat: 22.3038945, lng: 72.8200};
            //  console.log(myLatLng);
            var infoWindow = new google.maps.InfoWindow();
            var latlngbounds = new google.maps.LatLngBounds();
            var map = new google.maps.Map(document.getElementById("map"), mapOptions);
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(dLat, dLong),
                map: map,
                title: 'Hello World!',
                draggable: true
        });
            
               google.maps.event.addListener(marker, 'dragend', function(e) {
                var latlng = new google.maps.LatLng(e.latLng.lat(), e.latLng.lng());
                var geocoder = geocoder = new google.maps.Geocoder();
                geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[1]) {
                              // alert("Location: " + results[1].formatted_address + "\r\nLatitude: " + e.latLng.lat() + "\r\nLongitude: " + e.latLng.lng());
         document.getElementById('lat-span').innerHTML = e.latLng.lat();
        document.getElementById('lon-span').innerHTML = e.latLng.lng();
        document.getElementById('address').innerHTML = results[1].formatted_address;
        document.getElementById('lat-span').click();
        document.getElementById('lon-span').click();
        document.getElementById('address').click();
                        }
                    }
                });
            });
 $(document).ready(function(){
      $(".lat-span").click(function(){
        var value = $(this).html();
            var input = $('#dLat');
            input.val(value);
      });
      $(".lon-span").click(function(){
        var value = $(this).html();
            var input = $('#dLong');
            input.val(value);
      });
       
       
}); 

 $( window ).on( "load", function() {
          var value = $(".defaultLong").html();
              var input = $('#dLong');        
              input.val(value);
          var value = $(".defaultLat").html();
              var input = $('#dLat');        
              input.val(value);                 
      });

    }


    </script>
<style type="text/css">
  #map {
    width: 800px;
    height: 300px;
}
</style>
 
 @endsection

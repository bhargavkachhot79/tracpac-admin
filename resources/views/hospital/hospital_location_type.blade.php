@extends('layouts.app')
@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">            
            <h1>Hospital Type Management</h1>
            <h6>Type</h6>
            
          </div>
          <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Admin Management</li>
            </ol>
          </div> -->
        </div>
      </div><!-- /.container-fluid -->
    </section>
    @if (session('status'))
        <div class="alert alert-success center success">
              {{ session('status') }}
        </div>
     @endif

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">


						Show   <select>
						  <option value="10">10</option>
						  <option value="25">25</option>
						  <option value="50">50</option>
						  <option value="100">100</option>
						</select> entries
						 
						<div class="dt-buttons float-right col-sm-8">
							<a class="btn btn-default btn-primary btn-sm selectall"><span>Select all</span></a>
							 
							<a class="btn btn-default btn-sm deselect" ><span>Deselect all</span></a>
							<a class="btn btn-default btn-danger btn-sm delbtn delete" ><span>Delete</span></a>
							<a class="btn btn-default btn-success btn-sm activeinactive" ><span>Active/Inactive</span></a>
 


							<div style="float: right;">	 
								<form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form> 
        </div>
						</div>
						

						
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Type</th>
                  <th>Created Date &amp; Time</th>                   
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                   @if(count($location_types))                   
                    @foreach($location_types as $location_type)
                <tr>  
                  <td>{{ $location_type->vType }}</td>
                  <td>{{ $location_type->dtAdded }}</td>                   
                  <td>
                  	@if($location_type->eStatus == 'Active')
                    <a href="#inactive-{{$location_type->iTypeID }}" data-toggle="modal" class="btn btn-default btn-primary btn-sm selectall"><span>Active</span></a>
                     <div class="modal" id="inactive-{{$location_type->iTypeID }}" tabindex="-1" role="dialog">
                              <div class="modal-dialog" role="document">
                                 <div class="modal-content">
                                     
                                     <form action="{{route('inactive.location&type',$location_type->iTypeID)}}" method="POST">
                                       {{ csrf_field() }}
                                    <div class="modal-header">
                                       <h4 class="modal-title">Inactivate Location Agent</h4>
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                       <span aria-hidden="true">&times;</span>
                                       </button>
                                    </div>
                                    <div class="modal-body">
                                       <label>Are you sure you want to inactivate location agent?</label>
                                    </div>
                                    <div class="modal-footer">
                                       <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">No</button>
                                       <input type="submit" class="btn btn-primary" value="Yes" />
                                      </form>
                                    </div>
                                    
                                 </div>
                              </div> 
                    @else
                    <a href="#active-{{$location_type->iTypeID }}" data-toggle="modal" class="btn btn-default btn-primary btn-sm selectall"><span>Inactive</span></a>
                   
                     <div class="modal" id="active-{{$location_type->iTypeID }}" tabindex="-1" role="dialog">
                              <div class="modal-dialog" role="document">
                                 <div class="modal-content">
                                     
                                     <form action="{{route('active.location&type',$location_type->iTypeID)}}" method="POST">
                                       {{ csrf_field() }}
                                    <div class="modal-header">
                                       <h4 class="modal-title">Activate Location Agent</h4>
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                       <span aria-hidden="true">&times;</span>
                                       </button>
                                    </div>
                                    <div class="modal-body">
                                       <label>Are you sure you want to activate location agent?</label>
                                    </div>
                                    <div class="modal-footer">
                                       <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">No</button>
                                       <input type="submit" class="btn btn-primary" value="Yes" />
                                      </form>
                                    </div>
                                    
                                 </div>
                              </div> 
                     @endif
                  </td>
                  <td>
                  	<a href="{{route('edit.location&type',$location_type->iTypeID)}}" class="btn btn-default btn-primary btn-sm selectall"><span><i class="fas fa-edit"></i> Edit</span></a>
                    <a href="#delete-{{$location_type->iTypeID }}" data-toggle="modal" class="btn btn-default btn-danger btn-sm delbtn delete" ><span><i class="fa fa-trash" aria-hidden="true"></i> Delete</span></a>
                     <div class="modal" id="delete-{{$location_type->iTypeID }}" tabindex="-1" role="dialog">
                              <div class="modal-dialog" role="document">
                                 <div class="modal-content">
                                     
                                     <form action="{{route('destroy.location&type',$location_type->iTypeID)}}" method="POST">
                                       {{ csrf_field() }}
                                    <div class="modal-header">
                                       <h4 class="modal-title">Delete user</h4>
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                       <span aria-hidden="true">&times;</span>
                                       </button>
                                    </div>
                                    <div class="modal-body">
                                       <label>Are You Sure You Want To Delete?</label>
                                    </div>
                                    <div class="modal-footer">
                                       <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">No</button>
                                       <input type="submit" class="btn btn-primary" value="Yes" />
                                      </form>
                                    </div>
                                    
                                 </div>
                              </div>
                  </td>
                </tr>
                @endforeach
                  @else 
                    <tr><td colspan="6">No users found</td></tr>
                  @endif                   
                </tbody>
                <tfoot>
                <tr>
                  <th>Type</th>
                  <th>Created Date &amp; Time</th>                   
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
             	 <h6> Showing 1 to {{count($location_types)}} of {{count($location_types)}} entries</h6>
            </div>

            <!-- /.card-body -->
          </div>
          <!-- /.card -->          
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  @endsection
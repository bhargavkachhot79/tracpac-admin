@extends('layouts.app')
@section('content')
 
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Program And Program Head Management</h1>
          </div>
          <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Advanced Form</li>
            </ol>
          </div> -->
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <form action="{{route('update.program', $iProgramID )}}" method="POST">
           {{ csrf_field() }} 
          <div class="card-header">
            <h3 class="card-title">Edit Program</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                @foreach($program as $value)
                <!-- div class="form-group">                    
                  <label>School :</label>
                  <input type="text" value="{{$value->tbl_school['vSchoolName']}}" name="vSchoolName" class="form-control my-colorpicker1" placeholder="First name...">                
                </div> -->
                <!-- /.form-group -->
                <div class="form-group">                    
                  <label>Program :</label>
                  <input type="text" value="{{$value->vProgramName}}" name="vProgramName" class="form-control my-colorpicker1" placeholder="Program...">                
                </div>
                <div class="form-group">                    
                  <label>Email :</label>
                  <input type="email" disabled="disabled"  value="{{$value->tbl_program_head['vEmail']}}" name="vEmail" class="form-control disabled my-colorpicker1" placeholder="Email...">
                </div>
                <div class="form-group">                    
                  <label>Description :</label>                   
                    <textarea class="form-control my-colorpicker1" value="{{$value->tDescription}}" name="tDescription" placeholder="Program Description..." rows="4">{{$value->tDescription}}</textarea>               
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="form-group">                    
                  <label>Administrator First Name:</label>
                  <input type="text" value="{{$value->tbl_program_head['vFirstName']}}" name="vFirstName" class="form-control my-colorpicker1" placeholder="Administrator First Name...">                
                </div>
                <!-- /.form-group -->
                <div class="form-group">                    
                  <label>Administrator Last Name :</label>
                  <input type="text" value="{{$value->tbl_program_head['vLastName']}}" name="vLastName" class="form-control my-colorpicker1" placeholder="Administrator Last Name...">                
                </div>
                 
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
               @endforeach
            </div>
            <!-- /.row -->
 
          </div>
          <!-- /.card-body -->
          <div class="card-footer">                      
              <a class="btn btn-default btn-primary btn-sm selectall left" href="{{route('home')}}"><span>Cancle</span></a>
               <input type="submit" value="Save" class="btn btn-default btn-primary btn-sm selectall right">
          </div>
          </form>
        </div>
        <!-- /.card -->
 
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div> 
<!-- Page script -->
 
 @endsection

@extends('layouts.app')
@section('content')
 
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Subscription Management</h1>
          </div>
          <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Advanced Form</li>
            </ol>
          </div> -->
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Add Subscription</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">                    
                  <label class="required">School  </label>
                  <input type="text" class="form-control my-colorpicker1" placeholder="First name...">
                    </div>
                  <label class="required">Access For</label>
                 <div class="form-group"> 
                 <div class="row">
					 <div class="col-sm-3">
					  	<input type="radio" name="months" value="12" checked>  <label>12 Months</label>
					 </div>
					 <div class="col-sm-3">
					   <input type="radio" name="months" value="6">  <label>6 Months</label> 
					</div>
					<div class="col-sm-3"> 
					<input type="radio" name="months" value="6">  <label>Both</label>  
					</div>
					<div class="col-sm-3"> 
					</div>
    
				</div>                          
                                   
                       
                </div>
                 
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="form-group">                    
                  <label class="required">Program  </label>
                  <input type="text" class="form-control my-colorpicker1" placeholder="Email...">
                </div>
                <div class="form-group">                    
                  <label class="required">12 Months Amount  </label>
                  <input type="text" class="form-control my-colorpicker1" placeholder="Amount...">
                </div>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
 
          </div>
          <!-- /.card-body -->
          <div class="card-footer">                      
              <a class="btn btn-default btn-primary btn-sm selectall left" href="{{route('home')}}"><span>Cancle</span></a>
              <a class="btn btn-default btn-primary btn-sm selectall right"><span> Save</span></a>
          </div>
        </div>
        <!-- /.card -->
 
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div> 
<!-- Page script -->
 
 @endsection

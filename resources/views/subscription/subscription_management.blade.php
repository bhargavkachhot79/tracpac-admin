@extends('layouts.app')
@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
                    
            <h1>Subscription Management</h1>
            <h6>Subscription name</h6>
            
          </div>
          <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Admin Management</li>
            </ol>
          </div> -->
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">


						Show   <select>
						  <option value="10">10</option>
						  <option value="25">25</option>
						  <option value="50">50</option>
						  <option value="100">100</option>
						</select> entries
						 
						<div class="dt-buttons float-right col-sm-8">
							<a class="btn btn-default btn-primary btn-sm selectall"><span>Select all</span></a>
               
              <a class="btn btn-default btn-sm deselect" ><span>Deselect all</span></a>
               
              <a class="btn btn-default btn-success btn-sm activeinactive" ><span>Active/Inactive</span></a> 
 


							<div style="float: right;">	 
								<form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form> 
        </div>
						</div>
						

						
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>School Name</th>                  
                  <th>Program Name</th>
                  <th>12 Months Amount</th>
                  <th>6 Months Amount</th>
                  <th>Created Date &amp; Time</th>                   
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td>Trident</td>
                  <td>Internet
                    Explorer 4.0
                  </td>
                  <td>Win 95+</td>
                  <td>Win 95+</td>
                  <td>Win 95+</td>                   
                  <td>
                  	<a class="btn btn-default btn-primary btn-sm selectall"><span> Active</span></a>
                  </td>
                  <td>
                  	<a class="btn btn-default btn-primary btn-sm selectall"><span><i class="fas fa-edit"></i> Edit</span></a>
                  	<a class="btn btn-default btn-danger btn-sm delbtn delete" ><span><i class="fa fa-trash" aria-hidden="true"></i> Delete</span></a>	

                  </td>
                </tr>
                <tr>
                  <td>Trident</td>
                  <td>Internet
                    Explorer 5.0
                  </td>
                  <td>Win 95+</td>
                  <td>5</td>
                  <td>C</td>
                </tr>
                <tr>
                  <td>Trident</td>
                  <td>Internet
                    Explorer 5.5
                  </td>
                  <td>Win 95+</td>
                  <td>5.5</td>
                  <td>A</td>
                </tr>
                <tr>
                  <td>Trident</td>
                  <td>Internet
                    Explorer 6
                  </td>
                  <td>Win 98+</td>
                  <td>6</td>
                  <td>A</td>
                </tr>
                <tr>
                  <td>Trident</td>
                  <td>Internet Explorer 7</td>
                  <td>Win XP SP2+</td>
                  <td>7</td>
                  <td>A</td>
                </tr>
                <tr>
                  <td>Trident</td>
                  <td>AOL browser (AOL desktop)</td>
                  <td>Win XP</td>
                  <td>6</td>
                  <td>A</td>
                </tr>
                <tr>
                  <td>Gecko</td>
                  <td>Firefox 1.0</td>
                  <td>Win 98+ / OSX.2+</td>
                  <td>1.7</td>
                  <td>A</td>
                </tr>
                <tr>
                  <td>Gecko</td>
                  <td>Firefox 1.5</td>
                  <td>Win 98+ / OSX.2+</td>
                  <td>1.8</td>
                  <td>A</td>
                </tr>
                <tr>
                  <td>Gecko</td>
                  <td>Firefox 2.0</td>
                  <td>Win 98+ / OSX.2+</td>
                  <td>1.8</td>
                  <td>A</td>
                </tr> 
                </tbody>
                <tfoot>
                <tr>
                  <th>School Name</th>                  
                  <th>Program Name</th>
                  <th>12 Months Amount</th>
                  <th>6 Months Amount</th>
                  <th>Created Date &amp; Time</th>                   
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
             	 <h6> Showing 1 to 7 of 7 entries</h6>
            </div>

            <!-- /.card-body -->
          </div>
          <!-- /.card -->          
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  @endsection
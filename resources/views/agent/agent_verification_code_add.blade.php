@extends('layouts.app')
@section('content')
 
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Location Agent Management</h1>
          </div>
          <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Advanced Form</li>
            </ol>
          </div> -->
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <form action="{{route('store.Agent_verifiaction_code' )}}" method="POST">
           {{ csrf_field() }} 
          <div class="card-header">
            <h3 class="card-title">Add Location Agent</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                
                 <div class="form-group">
                  <label>School<span class="required">*</span></label>
                  <select name = "iSchoolID" class="form-control select2" required>
                    <option>Select School</option>
                    @foreach ($schools as $school)
                    <option value="{{ $school->iSchoolID }}">{{ $school->vSchoolName}}</option>
                    @endforeach 
                  </select>
                </div>
                <!-- /.form-group -->
                <div class="form-group">                    
                  <label>Code<span class="required">*</span></label>
                  <input type="text" name="vCode" class="form-control my-colorpicker1" placeholder="Verification Code...">                
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                
                <div class="form-group">                    
                  <label>Full Name :</label>
                  <input type="text" name="vAgentFullName" class="form-control my-colorpicker1" placeholder="Agent Full Name...">                
                </div>
                <!-- /.form-group -->
                
                <div class="form-group">                    
                  <label>Email<span class="required">*</span></label>
                  <input type="email" name="vAgentEmail" class="form-control my-colorpicker1" placeholder="Agent Email...">                
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
 
          </div>
          <!-- /.card-body -->
          <div class="card-footer">                      
              <a class="btn btn-default btn-primary btn-sm selectall left" href="{{route('home')}}"><span>Cancle</span></a>
              <input type="submit" class="btn btn-default btn-primary btn-sm selectall right">
          </div>
        </form>
       </div>
        </div>
        <!-- /.card -->
 
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div> 
<!-- Page script --> 
 @endsection

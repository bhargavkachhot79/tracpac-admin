<?php

namespace App;
use App\tbl_school;
use App\tbl_program;
use App\tbl_term_group;
use App\tbl_purchase;
use Illuminate\Database\Eloquent\Model;

class tbl_student extends Model
{
    protected $table = 'tbl_student';

    public function tbl_school()
	{
	    return $this->belongsTo('App\tbl_school','iSchoolID','iSchoolID');
	    
	}
	public function tbl_program()
	{
	    return $this->belongsTo('App\tbl_program','iProgramID','iProgramID');
	}
	public function tbl_term_group()
	{
	    return $this->belongsTo('App\tbl_term_group','iGroupID','iGroupID');
	}
	public function tbl_purchase()
	{
	    return $this->belongsTo('App\tbl_purchase','iStudentID','iStudentID');
	}
}

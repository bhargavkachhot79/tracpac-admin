<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_program_head extends Model
{
    const CREATED_AT = 'dtAdded';
    const UPDATED_AT = 'dtUpdate';
   protected $table = 'tbl_program_head';
}

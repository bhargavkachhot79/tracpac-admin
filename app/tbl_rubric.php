<?php

namespace App;
use App\tbl_program_head;
use Illuminate\Database\Eloquent\Model;

class tbl_rubric extends Model
{
    protected $table = 'tbl_rubric';
    const CREATED_AT = 'dtAdded';
    const UPDATED_AT = 'dtUpdate';

    public function tbl_program_head()
	{
	    return $this->belongsTo('App\tbl_program_head','iHeadID','iHeadID');
	    
	}
}

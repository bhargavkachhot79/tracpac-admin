<?php

namespace App\Http\Controllers;
use App\tbl_student;
use App\tbl_instructor;
use App\tbl_remediation;
use Illuminate\Http\Request;

class RemediationController extends Controller
{
    /**
     * Create a new controller instance.
     * 
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
     public function Remediation()
    {
         $remediations = tbl_remediation::with('tbl_instructor')->with('tbl_student')->get(); 
        return view('remediation.remediation',['remediations'=>$remediations]);
    }

// View Instructor 

    public function ViewRemediation( Request $request ,$iRemediationID )     {
        $remediation = tbl_remediation::with('tbl_instructor')->with('tbl_student')->where('iRemediationID',$iRemediationID)->get();  
        return view('remediation.remediation_view',compact('remediation','iRemediationID'));
     }

}

<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\tbl_hospital_type;
use App\tbl_hospital;

class HospitalController extends Controller
{
    /**
     * Create a new controller instance.
     * 
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    public function Hospital(Request $request)
    { 
        if($request->entries == null){
            $paginate = 10;
        }else{            
            $paginate = $request->entries;
        }
        $Allhospitals = tbl_hospital::with('tbl_hospital_type')->get();
        $hospitals = tbl_hospital::with('tbl_hospital_type')->orderBy('dtUpdate', 'desc')->paginate($paginate);
        return view('hospital.hospital',compact('paginate','paginate'),['hospitals'=>$hospitals,'Allhospitals'=>$Allhospitals]);
    }

 // Add Hopsital

    public function HospitalAdd()
    {
        return view('hospital.hospital_add');
    }

 // Store Hopsital

    public function Storehospitaltype(Request $request)
    {
        $checkbox = Input::get('checkbox',[]);
        $tTypeID = implode( ',',$checkbox );
        $hospital  = new tbl_hospital();        
        $hospital->vHostpitalName   = $request->vHostpitalName;
        $hospital->tAddress    = $request->tAddress;        
        $hospital->dLong    = $request->dLong;
        $hospital->dLat  = $request->dLat;
        $hospital->tTypeID  = $tTypeID;
        $hospital->iOrder = 0;               
        $hospital->save();        
        return redirect()->route('hospital')->with('status', 'Hospital added sucessfully!');        
    }
    
// Edit Hospital 

    public function Edithospitaltype( Request $request ,$iHospitalID )     {
        $hospital = tbl_hospital::with('tbl_hospital_type')->where('iHospitalID',$iHospitalID)->get();    
        $hospital_types = tbl_hospital_type::all(); 
        return view('hospital.hospital_edit',compact('hospital','iHospitalID'),['hospital_types'=>$hospital_types]);
     }

// Update Hospital

    public function Updatehospitaltype(Request $request,$iHospitalID)
    {   
        $checkbox = Input::get('checkbox',[]);
        $tTypeID = implode( ',',$checkbox );                 
        $hospital = tbl_hospital::where('iHospitalID',$iHospitalID)->update([
            'vHostpitalName' => $request->vHostpitalName,
            'tAddress' => $request->tAddress,
            'dLong' => $request->dLong,
            'dLat' => $request->dLat,
            'tTypeID' => $tTypeID,
            'iOrder' => 0,

        ]); 
          
        return redirect()->route('hospital')->with('status', 'Hospital Updated sucessfully!');
    }    

// Hospital Import   

    public function HospitalImport()
    {
        return view('hospital.hospital_import');
    }

// Location type

     public function LocationType()
    {
        $location_types = tbl_hospital_type::get();   
        return view('hospital.hospital_location_type',['location_types'=>$location_types]);
    }

// Activate Location type

    public function ActiveLocationtype($iTypeID)
    {

        $location = tbl_hospital_type::where('iTypeID',$iTypeID)->update([
            'eStatus' => 'Active',     
        ]);          
        return redirect()->route('hospital.location_type')->with('status', 'Location type Activated sucessfully!');        
    }

// Inactivate Location type

    public function InactiveLocationtype($iTypeID)
    {
        $agent = tbl_hospital_type::where('iTypeID',$iTypeID)->update(['eStatus' => 'Inactive',     
        ]);          
        return redirect()->route('hospital.location_type')->with('status', 'Location type Inactivated sucessfully!');        
    }

// Add Location type

     public function HospitalAddLocationType()
    {
        return view('hospital.hospital_add_location_type');
    }    

// Store Location type 

    public function StoreLocationType(Request $request)
    { 
        $loaction  = new tbl_hospital_type();       
        $loaction->vType  = $request->vType;                      
        $loaction->save();        
        return redirect()->route('hospital.location_type')->with('status', 'Location type added sucessfully!');        
    }

// Edit Location type 

    public function EditLocationtype( Request $request ,$iTypeID )     {
        $location = tbl_hospital_type::where('iTypeID',$iTypeID)->get();       
        return view('hospital.location_type_edit',compact('location','iTypeID'));
     }

// Update Location type

    public function UpdateLocationtype(Request $request,$iTypeID)
    {                    
        $hospital = tbl_hospital_type::where('iTypeID',$iTypeID)->update([
            'vType' => $request->vType,
        ]);          
        return redirect()->route('hospital.location_type')->with('status', 'Location type Updated sucessfully!');
    } 

// Delete Location type

    public function DeleteLocationtype ( Request $request ,$iTypeID )
     {  
         $agent = tbl_hospital_type::with('schoolDetails')->where('iTypeID',$iTypeID)->delete();
        $schools = tbl_school::all(); 
        return view('agent.agent_verification_code_edit',compact('agent','iAgentID'),['schools'=>$schools]);
       
     }
// Destroy Location type

     public function DestroyLocationtype($iTypeID)
    {

        $agent = tbl_hospital_type::where('iTypeID',$iTypeID)->delete(); 
        return redirect()->route('hospital.location_type')->with('status', 'Location type deleted sucessfully!');        
    }   


}

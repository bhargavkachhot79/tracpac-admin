<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_pagecontent;
class ContentController extends Controller
{
  	/**
     * Create a new controller instance.
     * 
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
  	 public function Content()
    {
        $contents = tbl_pagecontent::get(); 
        return view('content.content',['contents'=>$contents]);
    }

    public function EditContent(Request $request ,$iPageID)
    {
        $contents = tbl_pagecontent::where('iPageID',$iPageID)->get();  
        return view('content.content_edit',compact('contents','iPageID'),['contents'=>$contents]);
    }

// Update Content

    public function UpdateContent(Request $request,$iPageID)
    {   

        $agent = tbl_pagecontent::where('iPageID',$iPageID)->update([
            'vPageTitle' => $request->vPageTitle,
            'tContent' => $request->tContent,     
        ]); 
        
        return redirect()->route('content')->with('status', 'Content Updated sucessfully!');
    }
}

<?php

namespace App\Http\Controllers;
use App\tbl_evaluation;
use App\tbl_instructor;
use App\tbl_hospital;
use Illuminate\Http\Request;

class EvaluationController extends Controller
{
    /**
     * Create a new controller instance.
     * 
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
     
      public function Evaluation()
    {
         $evaluations = tbl_evaluation::with('tbl_instructor')->with('tbl_hospital')->get();   
        return view('evaluation.evaluation',['evaluations'=>$evaluations]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_clinical_wallet;
use App\tbl_student;
class ClinicalWalletController extends Controller
{
    /**
     * Create a new controller instance.
     * 
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
     public function ClinicalWallet()
    {
        $clinical_wallets = tbl_clinical_wallet::with('tbl_student')->get(); 
        return view('clinical_wallet.clinical_wallet',['clinical_wallets'=>$clinical_wallets]);
    }
}

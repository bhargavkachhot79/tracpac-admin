<?php

namespace App\Http\Controllers;
use App\tbl_admins;
use Illuminate\Http\Request;
use Auth;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return redirect()->route('admin.login',compact('admins'));
    }
    public function dashboard()
    {
        return view('admin-home');
    }
    public function Admin()
    {
        $admins = tbl_admins::orderBy('dAddeddateTime', 'DESC')->get();          
        return view('admin.admin',['admins'=>$admins]);
    }
     public function AdminAdd()
    {
        return view('admin.admin_add');
    }

 // Edit Admin

    public function EditAdmin ( Request $request ,$iAdminID )
     {
        $admin = tbl_admins::where('iAdminID',$iAdminID)->get(); 
        return view('admin.edit_admin',compact('admin','iAdminID'));
     }

// Destroy Admin

     public function DestroyAdmin($iAdminID)
    {

        $admin = tbl_admins::where('iAdminID',$iAdminID)->delete(); 
        return redirect()->route('admin')->with('status', 'Admin deleted sucessfully!');        
    }

// Store Admin

    public function StoreAdmin(Request $request)
    {

        $validatedData = $request->validate([
        
       'vFirstName' => 'required',
        'vLastName' => 'required',
        'vEmail' => 'required|email',
        'vPassword' =>  'required|min:6', 
         
        ]);
         
        $admin               = new tbl_admins();
        $admin->vFirstName   = $request->vFirstName;
        $admin->vLastName    = $request->vLastName;
        $admin->vEmail    = $request->vEmail;
        $admin->vPassword  = bcrypt($request->vPassword);                
        $admin->save();        
        return redirect()->route('admin')->with('status', 'Admin added sucessfully!');
        
    }    

// Update Admin

    public function UpdateAdmin(Request $request,$iAdminID)
    {   
        $validatedData = $request->validate([
        
       'vFirstName' => 'required',
        'vLastName' => 'required',
        'vEmail' => 'required|email',
        'vPassword' =>  'required|min:6', 
         
        ]);      


        $agent = tbl_admins::where('iAdminID',$iAdminID)->update([
            'vFirstName' => $request->vFirstName,
            'vLastName' => $request->vLastName,
            'vEmail' => $request->vEmail,
            'vPassword' => bcrypt($request->vPassword),    
        ]); 
        
        return redirect()->route('admin')->with('status', 'Admin Updated sucessfully!');
    }    

}

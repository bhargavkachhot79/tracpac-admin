<?php

namespace App\Http\Controllers;
use App\tbl_program;
use App\tbl_program_head;
use App\tbl_school;
use Illuminate\Http\Request;

class ProgramController extends Controller
{
    /**
     * Create a new controller instance.
     * 
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
     public function Program()
    {
        $programs = tbl_program::with('tbl_program_head')->with('tbl_school')->orderBy('dtUpdate', 'desc')->get();          
        return view('program.program',['programs'=>$programs]);

    }

// Edit Program 

    public function EditProgram( Request $request ,$iProgramID )     {
        $program = tbl_program::where('iProgramID',$iProgramID)->get(); 
        return view('program.program_edit',compact('program','iProgramID'));
     }

// Update Program

    public function UpdateProgram(Request $request,$iProgramID)
    {        
          
        $program = tbl_program::with('tbl_program_head')->with('tbl_school')->where('iProgramID',$iProgramID)->update([
            'vProgramName' => $request->vProgramName,
            'tDescription' => $request->tDescription,             
            ]); 
        $iHeadID = tbl_program::with('tbl_program_head')->where('iProgramID',$iProgramID)->pluck('iHeadID') ;
        $program = tbl_program_head::where('iHeadID',$iHeadID)->update([
            'vFirstName' => $request->vFirstName,
            'vLastName' => $request->vLastName, 
            ]);        
        return redirect()->route('program')->with('status', 'program Updated sucessfully!');
    }

// Destroy Program

     public function DestroyProgram($iProgramID)
    {
        $program = tbl_program::where('iProgramID',$iProgramID)->delete(); 
        return redirect()->route('program')->with('status', 'Program deleted sucessfully!');        
    } 

// Add Program

    public function ProgramAdd()
    {
        $schools = tbl_school::all();
        return view('program.program_add',['schools'=>$schools]);
    }

// Store Program    
 
    public function StoreProgram(Request $request)    { 

        $program_head = new tbl_program_head();
        $program_head->vFirstName   = $request->vFirstName;
        $program_head->vLastName    = $request->vLastName;
        $program_head->vEmail    = $request->vEmail;
        $program_head->tPastProgramids    = ''; 
        $program_head->iProgramID    = 0;
        $program_head->vPassword    = '';     
        $program_head->vImage    = ''; 
        $program_head->vGeneratetoken    = '';  
        $program_head->dtPasswordChange    = date('Y-m-d H:i:s', strtotime(null)); 
        $program_head->vFPwdToken    = '';   
        $program_head->dtPasswordForgotRequest    = date('Y-m-d H:i:s', strtotime(null)); 
        $program_head->dtPasswordForgot    = date('Y-m-d H:i:s', strtotime(null));              
        $program_head->save();       

        $program = new tbl_program();
        $program->iHeadID    = 0;
        $program->iSuperAdminID   = $request->iSuperAdminID;
        $program->vProgramName    = $request->vProgramName;
        $program->tDescription    = $request->tDescription;
        $program->eStatus    = 'Active';
        $program->isDeleted    = '0';                
        $program->save();

        $iProgramID = $program->id;
        $iHeadID = $program_head->id;        

        $program_head_update = tbl_program_head::where('iHeadID',$iHeadID)->update([
            'iProgramID' => $iProgramID, 
            ]);
        $program_update = tbl_program::where('iProgramID',$iProgramID)->update([
            'iHeadID' => $iHeadID, 
            ]);

        return redirect()->route('program')->with('status', 'Program added sucessfully!');        
    }

}

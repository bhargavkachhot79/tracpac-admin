<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\tbl_school;
use DateTime; 

class SchoolController extends Controller
{
    /**
     * Create a new controller instance.
     * 
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }


    public function School()
    {
        $schools = tbl_school::orderBy('dtUpdate', 'desc')->get();
        return view('school.school',['schools'=>$schools]);
    }

// Add School

    public function SchoolAdd()
    {
        return view('school.school_add');
    }

// Store School

    public function StoreSchool(Request $request)
    {

        $school  = new tbl_school();        
        $school->vSchoolName   = $request->vSchoolName;
        $school->tAddress    = $request->tAddress; 
        $school->vImage    = '';
        $school->vPassword    = '';        
        $school->vEmail    = $request->vEmail;
        $school->dLong    = $request->dLong;
        $school->dLat  = $request->dLat;
        $school->vZip  = $request->vZip;
        $school->vGeneratetoken    = '';
        $school->dtPasswordChange    = new DateTime(); 
        $school->vFPwdToken    = '';
        $school->dtPasswordForgotRequest    = new DateTime();
        $school->dtPasswordForgot    = new DateTime();    
        $school->save();                 
        return redirect()->route('school')->with('status', 'School added sucessfully!');        
    }

// Edit School 

    public function EditSchool( Request $request ,$iSchoolID )     {
        $school = tbl_school::where('iSchoolID',$iSchoolID)->get(); 
        return view('school.school_edit',compact('school','iSchoolID'));
     }

// Update School

    public function UpdateSchool(Request $request,$iSchoolID)
    {                   
        $school = tbl_school::where('iSchoolID',$iSchoolID)->update([
            'vSchoolName' => $request->vSchoolName,
            'vEmail' => $request->vEmail,
            'dLong' => $request->dLong,
            'dLat' => $request->dLat,
            'vZip' => $request->vZip, 
        ]);           
        return redirect()->route('school')->with('status', 'School Updated sucessfully!');
    }

// Delete Location type

    public function DeleteSchool (Request $request,$iSchoolID)
     {  
         $school = tbl_school::where('iSchoolID',$iSchoolID)->delete(); 
        return view('school.agent_verification_code_edit',compact('school','iSchoolID'));
       
     }
     
// Destroy Location type

     public function DestroySchool($iSchoolID)
    {

        $agent = tbl_school::where('iSchoolID',$iSchoolID)->delete(); 
        return redirect()->route('school')->with('status', 'School deleted sucessfully!');        
    }   

}

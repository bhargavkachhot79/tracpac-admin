<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SkillController extends Controller
{
    /**
     * Create a new controller instance.
     * 
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
     public function Skill(Request $request)
    {
        $paginate = $request->entries;         
         
        return view('skill.skill',compact('paginate','paginate'));
    }
    public function SkillAdd()
    {
        return view('skill.skill_add');
    }
    public function SkillImport()
    {
        return view('skill.skill_import');
    }
    public function ParticipationRole()
    {
        return view('skill.participation_role');
    }
}

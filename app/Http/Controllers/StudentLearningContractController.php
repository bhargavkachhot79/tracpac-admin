<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StudentLearningContractController extends Controller
{
     /**
     * Create a new controller instance.
     * 
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
     public function StudentLearningContract()
    {
        return view('student_learning_contract.student_learning_contract');
    }
}

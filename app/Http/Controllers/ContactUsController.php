<?php

namespace App\Http\Controllers;
use App\tbl_contactus;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
   	/**
     * Create a new controller instance.
     * 
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    public function ContactUs()
    {
        $contacts = tbl_contactus::get(); 
        return view('contact_us.contact_us',['contacts'=>$contacts]);
    }
}

<?php

namespace App\Http\Controllers; 
 
use Illuminate\Http\Request;
use App\tbl_location_agent; 
use App\Search;
use App\tbl_school;

class SearchController extends Controller
{
   public function Search(Request  $request)
    {
    	$search = $request->search;
        $AgentVerifications = tbl_location_agent::with(['schoolDetails'])->where('vAgentFullName','LIKE','%'.$search.'%')->get();
        // $AgentVerifications = tbl_location_agent::with(['schoolDetails'])->where('vAgentEmail','LIKE','%'.$search.'%')->get();        
        return view('search',['AgentVerifications'=>$AgentVerifications]);  
    }
}

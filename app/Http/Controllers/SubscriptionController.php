<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    /**
     * Create a new controller instance.
     * 
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    public function SubscriptionManagement()
    {
        return view('subscription.subscription_management');
    }
    public function SubscriptionManagementAdd()
    {
        return view('subscription.subscription_management_add');
    }
}

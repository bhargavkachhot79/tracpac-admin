<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_rubric;
use App\tbl_program_head;

class RubricController extends Controller
{
    /**
     * Create a new controller instance.
     * 
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
     public function Rubric()
    {
        $rubrics = tbl_rubric::with('tbl_program_head')->get();        
        return view('rubric.rubric',['rubrics'=>$rubrics]);
    }
}

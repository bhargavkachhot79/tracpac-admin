<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TermsController extends Controller
{
    /**
     * Create a new controller instance.
     * 
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    
     public function Terms()
    {
        return view('terms.terms');
    }
     public function TermsAdd()
    {
        return view('terms.terms_add');
    }
}

<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\tbl_admins;
use App\User;
use Hash;
use Auth;
use Redirect;
use Session;
use Validator;
use Illuminate\Support\Facades\Input;

class MainController extends Controller {

    public function login(Request $request){

        $rules = array(
            'vEmail' => 'required',
            'password' => 'required'
        );

        $vEmail = Input::get('vEmail');
        $password = Input::get('password');
        $model = tbl_admins::where('vEmail', $request->vEmail)->first();  
       
 

        
            return view('home');
         if (hash::check($password, $model->vPassword))

        {
        }
        else
        {
            return redirect()->route('login');
        }
    }
    public function register(Request $request)
    {
        $rules = array(
            'vFirstName' => 'required',
            'vLastName' => 'required',
            'vEmail' => 'required|unique:tbl_admins|email',
            'vPassword' => 'required|min:6'
        );
        $validator = Validator::make(Input::all() , $rules);

        if ($validator->fails())
        {
             return Redirect::back()->withErrors($validator, 'register')->withInput();
        }
        else
        {
            $user = new tbl_admins();
            $user->vFirstName = $request->get('vFirstName');
            $user->vLastName = $request->get('vLastName');
            $user->vEmail = $request->get('vEmail');
            $user->vPassword = Hash::make($request->get('password'));
            $user->save();
            return Redirect::back();
        }
    }
    public function logout()
    {
        Session::flush();
        Auth::logout();
        return Redirect::back();
    }
}


<?php

namespace App\Http\Controllers;
use App\tbl_instructor;
use App\tbl_school;
use App\tbl_program;
use Illuminate\Http\Request;

class InstructorController extends Controller
{
   /**
     * Create a new controller instance.
     * 
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
        
     public function Instructor()
    {
        $instructors =tbl_instructor::with('tbl_school')->orderBy('dtUpdate', 'desc')->get(); 
        return view('instructor.instructor',['instructors'=>$instructors]);
    }

// View Instructor 

    public function ViewInstructor( Request $request ,$iInstructorID )     {
        $instructor = tbl_instructor::with('tbl_school')->with('tbl_program')->where('iInstructorID',$iInstructorID)->get();  
        return view('instructor.instructor_view',compact('instructor','iInstructorID'));
     }

// Add Instructor

      public function InstructorAdd()
    {
        return view('instructor.instructor_add');
    }

// Edit Instructor 

    public function EditInstructor( Request $request ,$iInstructorID )     {
        $instructor = tbl_instructor::where('iInstructorID',$iInstructorID)->get(); 
        $schools = tbl_school::all();
        return view('instructor.instructor_edit',compact('instructor','iInstructorID'),['schools'=>$schools]);
     }

// Update Update 

    public function UpdateInstructor(Request $request,$iInstructorID)
    {         
        $instructor = tbl_instructor::where('iInstructorID',$iInstructorID)->update([ 
            ]);         
        return redirect()->route('instructor')->with('status', 'Instructor Updated sucessfully!');
    } 

// Destroy Instructor

     public function DestroyInstructor($iInstructorID)
    {
        $instructor = tbl_instructor::where('iInstructorID',$iInstructorID)->delete(); 
        return redirect()->route('instructor')->with('status', 'Instructor deleted sucessfully!');        
    }

    
}

<?php

namespace App\Http\Controllers;
use App\tbl_learning_center;
use Illuminate\Http\Request;

class LearningController extends Controller
{

    /**
     * Create a new controller instance.
     * 
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    
     public function LearningCenter()
    {
        $learnings = tbl_learning_center::get();   
        return view('learning.learning_center',['learnings'=>$learnings]);
    }
     public function LearningCenterAdd()
    {
        return view('learning.learning_center_add');
    }

}

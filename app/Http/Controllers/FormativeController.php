<?php

namespace App\Http\Controllers;
use App\tbl_eveluation_form;
use App\tbl_student_eveluation_form;
use Illuminate\Http\Request;

class FormativeController extends Controller
{
    /**
     * Create a new controller instance.
     * 
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

     public function FormativeForm()
    {
        $forms =tbl_eveluation_form::get();

        $school_total = array();
         $school_total = tbl_eveluation_form::with(['tbl_student_eveluation_form'])->get();
           foreach ($school_total as $key => $value) {
            $count = tbl_student_eveluation_form::where('iFormID',$value->iFormID)->count();           
            $forms[$key] = $value;
            $forms[$key]['count'] = $count;            
            $forms[$key][$value->iFormID] = $count;  
        }         
        return view('formative.formativeform',['forms'=>$forms]);
    }

    public function FormativeFormSchools()
    {
        $forms =tbl_eveluation_form::get();
        return view('formative.formativeform_details',['forms'=>$forms]);
    }
   
    public function FormativeFormAdd()
    {
        return view('formative.formativeform_add ');
    }
}

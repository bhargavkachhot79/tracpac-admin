<?php

namespace App\Http\Controllers;
use App\tbl_student;
use App\tbl_school;
use App\tbl_program;
use App\tbl_term_group;
use App\tbl_purchase;
use Illuminate\Http\Request;

class StudentController extends Controller
{

	/**
     * Create a new controller instance.
     * 
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

     public function Student()
    {
        $students = tbl_student::with('tbl_school')->with('tbl_program')->with('tbl_term_group')->get(); 
        return view('student.student',['students'=>$students]);
    }

// View Student 

    public function ViewStudent( Request $request ,$iStudentID )     {
        $student = tbl_student::with('tbl_school')->with('tbl_purchase')->with('tbl_program')->with('tbl_term_group')->where('iStudentID',$iStudentID)->get();  
        return view('student.student_view',compact('student','iStudentID'));
     }

// Destroy Student

     public function DestroyStudent($iStudentID)
    {

        $student = tbl_student::where('iStudentID',$iStudentID)->delete(); 
        return redirect()->route('student')->with('status', 'Student deleted sucessfully!');        
    }

     public function GraduatedStudents()
    {
        return view('student.graduated_students');
    }
     public function StudentAdd()
    {
        return view('student.student_add');
    }
}

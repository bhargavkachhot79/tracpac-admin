<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_location_agent;
use App\tbl_school;

class AgentController extends Controller
{
    /**
     * Create a new controller instance.
     * 
     * @return void
     */
  
    public function AgentVerification(Request $request)
    { 
        $paginate = $request->entries;
        $AgentVerifications = tbl_location_agent::with('schoolDetails')->orderBy('dtUpdate', 'desc')->paginate($paginate);
        $AllAgentVerifications = tbl_location_agent::with('schoolDetails')->get();
        return view('agent.agent_verification',compact('paginate','paginate'),['AgentVerifications'=>$AgentVerifications,'AllAgentVerifications'=>$AllAgentVerifications,'$paginate'=> $paginate]);
    }

// Create New Agent Verification Code
      
    public function AddAgentverificationcode(Request $request)
    {
        $schools = tbl_school::all(); 
        return view('agent.agent_verification_code_add',['schools'=>$schools]);
    }

// Edit Agent Verification Code 

    public function EditAgentverificationcode ( Request $request ,$iAgentID )
     {
        $agent = tbl_location_agent::with('schoolDetails')->where('iAgentID',$iAgentID)->get();
        $schools = tbl_school::all(); 
        return view('agent.agent_verification_code_edit',compact('agent','iAgentID'),['schools'=>$schools]);
     }

// Update Agent Verification Code

    public function UpdateAgentverificationcode(Request $request,$iAgentID)
    {   

        $agent = tbl_location_agent::where('iAgentID',$iAgentID)->update([
            'vAgentEmail' => $request->vAgentEmail,
            'vAgentFullName' => $request->vAgentFullName,
            'vCode' => $request->vCode,
            'iSchoolID' => $request->iSchoolID,     
        ]); 
        
        return redirect()->route('agent.verification')->with('status', 'Location agent Updated sucessfully!');
    }

// Store Agent Verification Code

    public function StoreAgentverificationcode(Request $request)
    {
         
        $agent               = new tbl_location_agent();
        $agent->vAgentEmail   = $request->vAgentEmail;
        $agent->vAgentFullName    = $request->vAgentFullName;
        $agent->vCode    = $request->vCode;
        $agent->iSchoolID  = $request->iSchoolID;                
        $agent->save();        
        return redirect()->route('agent.verification')->with('status', 'Location agent added sucessfully!');
        
    }
    
// Delete Agent Verification Code

    public function DeleteAgentverificationcode ( Request $request ,$iAgentID )
     {  
         $agent = tbl_location_agent::with('schoolDetails')->where('iAgentID',$iAgentID)->delete();
        $schools = tbl_school::all(); 
        return view('agent.agent_verification_code_edit',compact('agent','iAgentID'),['schools'=>$schools]);
       
     }
// Destroy Agent Verification Code

     public function DestroyAgentverificationcode($iAgentID)
    {

        $agent = tbl_location_agent::where('iAgentID',$iAgentID)->delete(); 
        return redirect()->route('agent.verification')->with('status', 'Location agent deleted sucessfully!');        
    }

// Activate Agent Verification Code

    public function ActiveAgentverificationcode($iAgentID)
    {

        $agent = tbl_location_agent::where('iAgentID',$iAgentID)->update([
            'eStatus' => 'Active',     
        ]);          
        return redirect()->route('agent.verification')->with('status', 'Location agent Activated sucessfully!');        
    }

// Inactivate Agent Verification Code

    public function InactiveAgentverificationcode($iAgentID)
    {

        $agent = tbl_location_agent::where('iAgentID',$iAgentID)->update([
            'eStatus' => 'Inactive',     
        ]);          
        return redirect()->route('agent.verification')->with('status', 'Location agent Inactivated sucessfully!');        
    }
}

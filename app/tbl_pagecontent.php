<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_pagecontent extends Model
{
    protected $table = 'tbl_pagecontent';
    const CREATED_AT = 'tCreatedAt';
    const UPDATED_AT = 'tModifiedAt';
}

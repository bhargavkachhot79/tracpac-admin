<?php

namespace App;
use App\tbl_instructor;
use App\tbl_hospital;
use Illuminate\Database\Eloquent\Model;

class tbl_evaluation extends Model
{
    const CREATED_AT = 'dtAdded';
    const UPDATED_AT = 'dtUpdate';
    protected $table = 'tbl_evaluation';


 public function tbl_hospital()
	{
	    return $this->belongsTo('App\tbl_hospital','iHospitalID','iHospitalID');
	    
	}
	public function tbl_instructor()
	{
	    return $this->belongsTo('App\tbl_instructor','iInstructorID','iInstructorID');
	}

}	

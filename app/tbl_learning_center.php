<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_learning_center extends Model
{
    const CREATED_AT = 'dtAdded';
    const UPDATED_AT = 'dtUpdate';  
    protected $table = 'tbl_learning_center'; 
}

<?php

namespace App;
use App\tbl_hospital_type;

use Illuminate\Database\Eloquent\Model;

class tbl_hospital extends Model
{
    const CREATED_AT = 'dtAdded';
    const UPDATED_AT = 'dtUpdate';  
    protected $table = 'tbl_hospital';

    public function tbl_program_head()
	{
	    return $this->belongsTo('App\tbl_hospital_type','tTypeID','iTypeID');
	}

	// public function hospitalDetails()
	// {
	//     return $this->belongsTo('App\tbl_hospital_type','tTypeID','iTypeID');
	// }
}

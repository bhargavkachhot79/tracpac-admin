<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_clinical_wallet extends Model
{
    const CREATED_AT = 'dtAdded';
    const UPDATED_AT = 'dtUpdate';
    protected $table = 'tbl_clinical_wallet';

    public function tbl_student()
	{
	    return $this->belongsTo('App\tbl_student','iStudentID','iStudentID');
	}
}

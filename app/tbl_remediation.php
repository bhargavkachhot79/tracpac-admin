<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_remediation extends Model
{
    const CREATED_AT = 'dtAdded';
    const UPDATED_AT = 'dtUpdate';
    protected $table = 'tbl_remediation';
    
    public function tbl_student()
	{
	    return $this->belongsTo('App\tbl_student','iStudentID','iStudentID');
	    
	}
	public function tbl_instructor()
	{
	    return $this->belongsTo('App\tbl_instructor','iInstructorID','iInstructorID');
	}
}

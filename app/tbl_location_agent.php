<?php

namespace App;
use App\tbl_school;
use Illuminate\Database\Eloquent\Model;

class tbl_location_agent extends Model
{
	 
    const CREATED_AT = 'dtAdded';
    const UPDATED_AT = 'dtUpdate';
    protected $table = 'tbl_location_agent';

     public function schoolDetails()
	{
	    return $this->belongsTo('App\tbl_school', 'iSchoolID', 'iSchoolID');
	}
}
 
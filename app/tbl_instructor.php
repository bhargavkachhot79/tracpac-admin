<?php

namespace App;
use App\tbl_school;
use App\tbl_program;
use App\tbl_term;

use Illuminate\Database\Eloquent\Model;

class tbl_instructor extends Model
{
	const CREATED_AT = 'dtAdded';
    const UPDATED_AT = 'dtUpdate';
    protected $table = 'tbl_instructor';

    public function tbl_school()
	{
	    return $this->belongsTo('App\tbl_school','iSchoolID','iSchoolID');
	}
	public function tbl_program()
	{
	    return $this->belongsTo('App\tbl_program','iProgramID','iProgramID');
	}
	public function tbl_term()
	{
	    return $this->belongsTo('App\tbl_term','iTermID','iTermID');
	}
}

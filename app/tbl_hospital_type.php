<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_hospital_type extends Model
{
    const CREATED_AT = 'dtAdded';
    const UPDATED_AT = 'dtUpdate';
    protected $table = 'tbl_hospital_type';
}

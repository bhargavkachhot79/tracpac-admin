<?php

namespace App;
use App\tbl_school;
use Illuminate\Database\Eloquent\Model;

class tbl_program extends Model
{
    const CREATED_AT = 'dtAdded';
    const UPDATED_AT = 'dtUpdate';
    protected $table = 'tbl_program';
    
    public function tbl_program_head()
	{
	    return $this->belongsTo('App\tbl_program_head','iHeadID','iHeadID');
	    
	}
	public function tbl_school()
	{
	    return $this->belongsTo('App\tbl_school','iSuperAdminID','iSchoolID');
	}
}

<?php

namespace App;
use App\tbl_student_eveluation_form;
use Illuminate\Database\Eloquent\Model;

class tbl_eveluation_form extends Model
{
     
    protected $table = 'tbl_eveluation_form';

    public function tbl_student_eveluation_form()
	{
	    return $this->belongsTo('App\tbl_student_eveluation_form','iFormID','iFormID');
	}
}
